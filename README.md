Cours ECR 9e HARMOS
===================

*date de création* : 2017/11/27


Destinataires du projet
-----------------------
Dépôt personnel. Je le laisse ouvert par simple principe de partage. Cependant,
je n'assure aucun service sur son contenu.

Le contenu de ce dépôt ne devrait pas être utile à grand monde. Il concerne un
programme scolaire particulier utilisé dans une petite zone géographique. Il
serait très étonnant qu'il y ait parmi les enseignants l'utilisant des gens
qui connaissent LaTeX, encore moins qui l'utilisent!


Ce que le projet fait
---------------------
Il s'agit, normalement, de script LaTeX. Ce sont des documents qu'il m'arrive
d'utiliser en classe en réponse à des questions d'élèves, ou en plus du
matériel de base fourni par l'école.


Prérequis
---------
### Matériels
- GNU/Linux


### Logiciel
- GIT
- Git Flow
- LaTeX2e
- VIM


Documentation
-------------

Il se peut que ces fichiers existent à la racine... Mais pas forcément (^_^)

1. [CHANGELOG](CHANGELOG.md) Changements importants d'une version à l'autre
2. [CONTRIBUTING](CONTRIBUTING.md) indications utiles pour les personnes qui
    voudraient contribuer au projet
3. [HISTORY](HISTORY.md) histoire du projet
   d'installation
4. [LICENSE](LICENSE) termes de la licence
5. [MANIFEST](MANIFEST) liste des fichiers


### Documentation généraliste
#### Git
- https://searx.laquadrature.net/?q=git&categories=general&language=fr

#### GitFlow
- https://searx.laquadrature.net/?q=gitflow&categories=general&language=fr

#### LaTex
- https://searx.laquadrature.net/?q=latex%20tex&categories=general&language=fr


