Conventions de nomnage
======================

Arborescence de fichier
-----------------------

### doc
Placez dans *doc* et ses sous-répertoires toute la documentation afférente au projet, sans oublier les notes et courriers électroniques importants. Vous pouvez avoir des sous-répertoires de doc contenant différents types de documents ou pour différentes phases du projet.

Si vous avez besoin de documentation externe, envisager de la copier ici. Cela rendra service pour maintenir le projet si l'endroit où les données en questions étaient accessibles disparaît.


### data
Certaines données sont nécessaires, par exemple pour peupler une table de correspondances dans une base de données. Il vaut mieux garder toutes ces données au même endroit.


### db
Si le projet utilise une base de données, placer dans *db* les schémas de la base en question. Essayez autant que possible de ne pas modifier les schémas directement dans la base.


### src
Ce répertoire contient le code source du projet. Vous pouvez y faire des sous-répertoires pour différents types de code source, par exemple:

* src/java
* src/php


### util
Répertoire contenant les utilitaires, outils et scripts spécifiques au projet.


### vendor
Si le projet utilise des bibliothèques fournies par une partie tierce ou des fichiers d'en-têtes que vous désirez archiver avec votre code, faites-le ici.


Gestionnaire de version
-----------------------
Le workflow git suit scrupuleusement git-flow (voir [CONTRIBUTING](CONTRIBUTING))


### Branche **master**
Elle représente le dernier état installable en production du projet. Seul le créateur du dépôt peut travailler travailler dans cette branche.


### Branche **devel**
La branche où est récolté le travail de tout le monde, des branches de développement privées. Seul la "Team" peut travailler dans cette branche.


### Les autres branches
Les branches, tags et versions qui sont poussées sur le serveur sont **toutes** documentées dans le dossier: '''/doc/git/(featrure|bugfix|release|hotfix|support|tags)/'''

Ce dossier doit contenir les fichiers suivants (par ordre d'importance):

1. [README](**README**) IMPÉRATIF, contenant:
	a. Raison d'être de la branche
	b. les points suivants s'ils ne justifient pas la mise d'un fichier, si non un
	lien vers le fichier.
2. [CHANGELOG](**CHANGELOG**) Changements importants d'une version à l'autre
3. [CONTRIBUTING](**CONTRIBUTING**) indication utiles pour les personnes qui voudraient contribuer au projet
4. [HISTORY](**HISTORY**) histoire du projet
5. les scripts de test.


### les branches **feature**
Chaque branche doit être Nommée de la manière suivante:

* PSEUDO-DESCRIPTION

où:

* **PSEUDO** est le pseudo de l'administrateur (le créateur) de la branche
* **DESCRIPTION** Une description en CamelCase (RaisonCreationBranche) de cette branche


